import { Injectable } from '@angular/core';

import * as url from 'url';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  initialized: boolean;
  initializedPromise: Promise<void>;
  initializedResolve;

  serverUrl: string;
  serverReachable: boolean;
  listeners: Array<any>;

  extraHeaders: () => any;

  constructor() {
    this.initialized = false;
    this.initializedPromise = new Promise((resolve, reject) => {
      this.initializedResolve = resolve;
    })
      .then((res) => {
        return undefined;
      })
      .catch((err) => {
        return undefined;
      })
      .finally(() => {
        this.initialized = true;
      });

    this.serverUrl = null;
    this.serverReachable = false;
    this.listeners = [];

    this.extraHeaders = () => ({});
  }

  initialize(serverUrl): Promise<void> {
    this.initialized = false;
    this.serverUrl = serverUrl;

    return this._request('ping', this.getConf())
      .then((res) => {
        return undefined;
      })
      .catch((err) => {
        return undefined;
      })
      .finally(() => {
        this.initializedResolve(undefined);
      });
  }

  addListener(listener) {
    this.listeners.push(listener);
  }

  fireEvent(event, ...args) {
    setTimeout(
      () => {
        this.listeners.forEach(listener => {
          if (listener[event] !== undefined) {
            listener[event](...args);
          }
        });
      },
      0
    );
  }

  resolve(_url, query = null) {
    _url = url.resolve(this.serverUrl, _url);

    if (query) {
      query = JSON.stringify(query);

      const searchParams = new URLSearchParams();
      searchParams.append('query', query);

      _url = _url + '?' + searchParams.toString();
    }

    return _url;
  }

  getConf() {
    return {
      method: 'GET',
      credentials: 'include',
      redirect: 'follow',
      referrer: 'no-referrer',
    };
  }

  postConf() {
    return {
      method: 'POST',
      cache: 'no-cache',
      credentials: 'include',
      redirect: 'follow',
      referrer: 'no-referrer',
    };
  }

  putConf() {
    return {
      method: 'PUT',
      cache: 'no-cache',
      credentials: 'include',
      redirect: 'follow',
      referrer: 'no-referrer',
    };
  }

  deleteConf() {
    return {
      method: 'DELETE',
      cache: 'no-cache',
      credentials: 'include',
      redirect: 'follow',
      referrer: 'no-referrer',
    };
  }

  async get(url: string, request: object = null): Promise<any> {
    if (!this.initialized) {
      await this.initializedPromise;
    }

    return this._request(url, this.getConf(), request);
  }

  async post(url: string, request: object = {}, files: Array<any> = []): Promise<any> {
    if (!this.initialized) {
      await this.initializedPromise;
    }

    return this._request(url, this.postConf(), request, files);
  }

  async put(url: string, request: object = {}, files: Array<any> = []): Promise<any> {
    if (!this.initialized) {
      await this.initializedPromise;
    }

    return this._request(url, this.putConf(), request, files);
  }

  async delete(url: string, request: object = {}, files: Array<any> = []): Promise<any> {
    if (!this.initialized) {
      await this.initializedPromise;
    }

    return this._request(url, this.deleteConf(), request, files);
  }

  async blob(url: string, request: object = {}): Promise<any> {
    if (!this.initialized) {
      await this.initializedPromise;
    }

    return this._request(url, this.postConf(), request);
  }

  _request(_url: string, config: any, request: any = null, files: Array<any> = []): Promise<any> {
    // CONTENT
    let headers;
    let body;

    // EXTRA HEADERS
    headers = this.extraHeaders();

    // SET HEADER AND BODY
    if (config.method === 'GET') {
      _url = this.resolve(_url, request);
    } else if (files.length === 0) {
      // URL
      _url = url.resolve(this.serverUrl, _url);

      // HB
      headers['Content-Type'] = 'application/json';
      body = (request ? JSON.stringify(request) : undefined);
    } else {
      // URL
      _url = url.resolve(this.serverUrl, _url);

      // BUILD FORMDATA
      const fd = new FormData();

      if (request) {
        fd.append('request', JSON.stringify(request));
      }

      for (const f of files) {
        fd.append(f.name, f.file, f.filename || f.file.name);
      }

      // HB
      body = fd;
    }

    // BUILD REQUEST
    config.headers = headers;
    config.body = body;

    // FETCH
    let reachable = this.serverReachable;

    return fetch(
      _url,
      config
    )
      .then(async res => {
        // IS REACHABLE
        reachable = true;

        // CHECK CONTENT TYPE
        const resContentType = res.headers.get('Content-Type');

        let isJSON = false;
        isJSON = isJSON || resContentType === null;
        isJSON = isJSON || resContentType === undefined;
        isJSON = isJSON || resContentType === 'text/plain';
        isJSON = isJSON || resContentType === 'application/json';

        if (isJSON) {
          // READ RESPONSE
          const jsonRes = await res.json();

          const message = new APIMessage(
            jsonRes
          );

          // CHECK ERROR
          if (message.iserror) {
            throw APIError.fromAPIMessage(message);
          } else {
            return message.payload;
          }
        } else {
          return res.blob();
        }
      })
      .catch(err => {
        if (err instanceof APIError) {
          // FIRE
          this.fireEvent('onError', err);

          // THROW
          throw err;
        }

        // NOT REACHABLE
        reachable = false;

        // THROW ERROR
        throw new APIError(-1, -1, 'No se pudo contactar al servidor.', {});
      })
      .finally(() => {
        if (this.serverReachable !== reachable) {
          this.serverReachable = reachable;

          if (this.serverReachable) {
            this.fireEvent('onReachable');
          } else {
            this.fireEvent('onUnreachable');
          }
        }
      });
  }
}

export class APIMessage {

  header: any;
  payload: any;

  iserror: boolean;

  constructor(message) {
    this.header = this._opt('header', {}, message);
    this.payload = this._opt('payload', {}, message);
    this.iserror = this._opt('__iserror__', false, this.header);
  }

  _opt(key, def, map) {
    if (key in map) {
      return map[key];
    } else {
      return def;
    }
  }
}

export class APIError extends Error {

  code: number;
  status: number;
  message: string;
  details: any;

  static fromAPIMessage(message) {
    return new this(
      message.payload.code,
      message.payload.status,
      message.payload.message,
      message.payload.details
    );
  }

  constructor(code: number, status: number, message: string, details: any) {
    super("ERROR " + code + ": " + message);

    Object.setPrototypeOf(this, APIError.prototype);

    this.code = code;
    this.status = status;
    this.message = message;
    this.details = details;
  }
}
