// EXTERNAL
import { BigNumber } from 'bignumber.js';
import * as moment from 'moment';

export function asBoolean(value: any): boolean {
    if (value === null) {
        return false;
    }

    if (typeof value === 'boolean') {
        return value;
    }

    value = String(value).toLowerCase().trim();

    switch (value) {
        case 'true': case 'yes': case '1':
            return true;
        case 'false': case 'no': case '0': case '':
            return false;
        default:
            return Boolean(value);
    }
}

export function asInteger(value: any): number {
    if (value === null) {
        return null;
    }

    if (typeof value === 'number') {
        return new BigNumber(value)
            .decimalPlaces(0, BigNumber.ROUND_HALF_UP)
            .toNumber();
    } else {
        value = asString(value);
    }

    if (value === null) {
        return null;
    }

    return parseInt(value, 10);
}


export function asFloat(value: any): number {
    if (value === null) {
        return null;
    }

    if (typeof value === 'number') {
        return value;
    } else {
        value = asString(value);
    }

    if (value === null) {
        return null;
    }

    return parseFloat(value);
}

export function asString(value: any): string {
    if (value === null) {
        return null;
    }

    if (typeof value === 'string') {
        value = value.trim();

        if (value === '') {
            return null;
        }

        return value;
    } else {
        value = String(value).trim();

        if (value === '') {
            return null;
        }

        return value;
    }
}

export function asDate(value: any): Date {
    if (value === null) {
        return null;
    }

    if (value instanceof Date) {
        return value;
    } else if (moment.isMoment(value)) {
        if (value.isValid()) {
            return value.toDate();
        }

        return value.toDate();
    } else if (typeof value === 'string') {
        value = asString(value);

        if (value === null) {
            return null;
        }
    }

    value = moment(value);

    if (value.isValid()) {
        return value.toDate();
    }

    throw new Error('Invalid date.');
}
