import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MariaCommonsComponent } from './maria-commons.component';

describe('MariaCommonsComponent', () => {
  let component: MariaCommonsComponent;
  let fixture: ComponentFixture<MariaCommonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MariaCommonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MariaCommonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
