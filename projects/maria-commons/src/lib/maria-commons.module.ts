import { NgModule } from '@angular/core';
import { MariaCommonsComponent } from './maria-commons.component';



@NgModule({
  declarations: [MariaCommonsComponent],
  imports: [
  ],
  exports: [MariaCommonsComponent]
})
export class MariaCommonsModule { }
