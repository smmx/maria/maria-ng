import { TestBed } from '@angular/core/testing';

import { MariaCommonsService } from './maria-commons.service';

describe('MariaCommonsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MariaCommonsService = TestBed.get(MariaCommonsService);
    expect(service).toBeTruthy();
  });
});
